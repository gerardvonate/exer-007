package org.example
import java.sql.DriverManager


fun main() {
    val jdbcUrl = "jdbc:postgresql://rds.dev.corporate.lexagle.com:5432/corporate"
    val username = "lexadmin"
    val password = "BestProductEver!23"

    val connection = DriverManager.getConnection(jdbcUrl, username, password)

    val EXER_002_QUERY = "SELECT organization_id, AVG(signatory_count_result.signatory_count) AS avg_no_of_signatory\n" +
            "FROM (SELECT COUNT(agreement_signatory.signatory_id) AS signatory_count, corporate.user.account.organization_id AS organization_id FROM\n" +
            "agreement.agreement_signatory LEFT JOIN corporate.user.account ON agreement.agreement_signatory.added_by = corporate.user.account.email\n" +
            "GROUP BY agreement_signatory.agreement_id, corporate.user.account.organization_id) \n" +
            "AS signatory_count_result \n" +
            "GROUP BY organization_id"

    val query_002 = connection.prepareStatement(EXER_002_QUERY)
    val result_002 = query_002.executeQuery()

    while(result_002.next()){
        val organizationId = result_002.getString("organization_id")
        val avgNoOfSignatory = result_002.getString("avg_no_of_signatory")
        println(String.format("%s-%s", organizationId, avgNoOfSignatory))
    }

    val EXER_007_QUERY = "SELECT COUNT(agreement_signatory.signatory_id) AS signatory_count, corporate.user.account.organization_id AS organization_id, agreement_signatory.agreement_id FROM\n" +
            "agreement.agreement_signatory LEFT JOIN corporate.user.account ON agreement.agreement_signatory.added_by = corporate.user.account.email\n" +
            "GROUP BY agreement_signatory.agreement_id, corporate.user.account.organization_id"

    val query_007 = connection.prepareStatement(EXER_007_QUERY)
    val result = query_007.executeQuery()

    val resultMap = HashMap<String, ArrayList<Int>>()
    while(result.next()) {
        val signatoryCount = result.getInt("signatory_count")
        val organizationId = result.getString("organization_id")

        if(!resultMap.containsKey(organizationId)) {
            resultMap.put(organizationId, arrayListOf<Int>())
        }
        resultMap[organizationId]?.add(signatoryCount)
    }

    for((key, value) in resultMap) {
        var averageNoOfSignatories = 0.0
        for(count in value) {
            averageNoOfSignatories += count;
        }
        averageNoOfSignatories /= value.size
        println(String.format("%s-%s", key, averageNoOfSignatories))
    }

    connection.close()
}